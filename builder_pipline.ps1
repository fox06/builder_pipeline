$ibcmd = "C:\Program Files\1cv8\8.3.15.1869\bin\ibcmd.exe"
$1cv8exe = "C:\Program Files\1cv8\8.3.15.1869\bin\1cv8.exe"
$work_dir = 'd:\root\pipe\temp\'
$new_dir = 'd:\root\pipe\updates\'
$builds_path = 'D:\root\builds\'
$src = 'D:\root\pipe\src'
$db_path = 'd:\root\pipe\temp\db'
$path_to_dataprocessor = "D:\root\UpdatesDiscriptionExctractor.epf"

# Set-ExecutionPolicy Bypass -Scope Process

Write-Output "Cleaning..."
Remove-Item -Path $work_dir -Recurse -Force -Confirm:$false

Write-Output 'Creating Temporary DB...'
$arg1c = "infobase create --db-path=$($db_path) --import=$($src) --apply"

$proc = Start-Process $ibcmd $arg1c -PassThru 
$proc.WaitForExit()

$xml_ver = [XML](Get-Content ${src}\Configuration.xml)
$src_version = $xml_ver.Metadataobject.Configuration.Properties.Version

$s = $src_version.split(".")
if($s[-1] -eq 0) {
    $s[-2] = $s[-2] - 1
}   
$s = $s[0..($s.Length-2)]
$mask = $s -join "."

Write-Output 'Copy last cf update files....'
$cf_files = Get-ChildItem -Path ${builds_path}\${mask}* -Filter "*.cf" -Recurse

$vers_cf_paths = ''
$cf_files | ForEach-Object { $vers_cf_paths+= " -f " + $_.FullName}



$vers_list = [System.Collections.Generic.List[string]]::new()
$cf_files | ForEach-Object {
    $d = Split-Path (Split-Path $_.FullName -parent) -Leaf
     $vers_list.Add($d)
    }

$vers = $vers_list.ToArray()

Write-Output 'Creating distribution files...'
$upd_dir = $new_dir + $src_version + "\"

$cf_file = $upd_dir + "1cv8.cf"
$cfu_file = $upd_dir + "1cv8.cfu"

$arg1c = "DESIGNER /F ""$($db_path)"" /Visible /CreateDistributionFiles -cffile ""$($cf_file)"" -cfufile ""$($cfu_file)"" $($vers_cf_paths)"

$proc = Start-Process $1cv8exe $arg1c -PassThru 
$proc.WaitForExit()

Write-Output 'Generating news.htm...'

$V83Com = New-Object -COMObject "V83.COMConnector"
$Connection = $V83Com.Connect("FILE=$($db_path)")

$ext_data_proc =[System.__ComObject].InvokeMember("ExternalDataProcessors",[System.Reflection.BindingFlags]::GetProperty,$null,$Connection,$null)

$new_ext_data_proc = [System.__ComObject].InvokeMember("Create",[System.Reflection.BindingFlags]::InvokeMethod,$null,$ext_data_proc, ($path_to_dataprocessor , $False))

$OutputHTMLFilePath = $upd_dir + "news.htm"

[System.__ComObject].InvokeMember("ExtractUpdateDiscription",[System.Reflection.BindingFlags]::InvokeMethod,$null,$new_ext_data_proc, ($OutputHTMLFilePath, $vers))


